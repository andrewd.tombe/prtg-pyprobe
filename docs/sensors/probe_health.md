---
**WARNING**  
Although this sensor can be added to every device on the probe, it will **ONLY** monitor the probe itself.  

---
#### Description
Sensor used to monitor the health of the system on which the python probe is running.

---
#### Configuration
**Choose between Celsius or Fahrenheit display**:  Choose whether you want to return the value in Celsius or Fahrenheit. When changed after sensor is created, the unit will disappear in the web interface.  
**Full Display of all disk space values**: Choose whether you want to get all disk space data or only percentages.

---
![Probe Health](../img/sensor_probehealth.png){: align=left }
