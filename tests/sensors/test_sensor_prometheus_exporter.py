import asyncio

import httpx
import pytest
from httpx import AsyncClient as Aclient
from httpx._models import Response

from prtg_pyprobe.sensors import sensor_prometheus_exporter as module
from prtg_pyprobe.sensors.sensor_prometheus_exporter import CastException, Sensor
from prtg_pyprobe.sensors.sensor import SensorBase


def task_data_prometheus_exporter():
    return {
        "sensorid": "1234",
        "port": "5000",
        "host": "127.0.0.1",
        "timeout": "10",
        "kind": "mpprometheusexporter",
        "metrics": "metric_without_timestamp_and_labels",
        "metric_path": "/metrics.txt",
    }


async def metrics(*args, **kwargs):
    with open("sensors/resources/metrics.txt", "r") as f:
        raw_metrics = f.read()
    resp = Response(status_code=200, text=raw_metrics)
    return resp


async def raise_httpx_httperror(*args, **kwargs):
    raise httpx.HTTPError(message="Something happened")


async def raise_key_error(*args, **kwargs):
    raise KeyError


async def raise_value_error(*args, **kwargs):
    raise ValueError


async def raise_casting_exception(*args, **kwargs):
    raise CastException(1, "test")


class TestPrometheusExporterSensorProperties:
    def test_sensor_prometheus_exporter_base_class(self, prometheus_exporter_sensor):
        assert type(prometheus_exporter_sensor).__bases__[0] is SensorBase

    def test_sensor_prometheus_exporter_name(self, prometheus_exporter_sensor):
        assert prometheus_exporter_sensor.name == "Prometheus Exporter (experimental)"

    def test_sensor_prometheus_exporter_kind(self, prometheus_exporter_sensor):
        assert prometheus_exporter_sensor.kind == "mpprometheusexporter"

    def test_sensor_prometheus_exporter_definition(self, prometheus_exporter_sensor):
        assert prometheus_exporter_sensor.definition == {
            "description": "Monitors a Prometheus exporter",
            "groups": [
                {
                    "caption": "Target Information",
                    "fields": [
                        {
                            "caption": "Timeout (in s)",
                            "default": 10,
                            "help": "If the reply takes longer than this value "
                            "the request is aborted and an error message "
                            "is triggered. Maximum value is 900 sec. (= "
                            "15.0 minutes)",
                            "maximum": 900,
                            "minimum": 10,
                            "name": "timeout",
                            "required": "1",
                            "type": "integer",
                        },
                        {
                            "caption": "Exporter Port",
                            "help": "Enter the port the Prometheus Exporter is " "listening on.",
                            "name": "port",
                            "required": "1",
                            "type": "edit",
                        },
                    ],
                    "name": "target_info",
                },
                {
                    "caption": "Metric Information",
                    "fields": [
                        {
                            "caption": "Metrics",
                            "default": "",
                            "help": "Enter a list of comma separated metrics to " "capture.",
                            "name": "metrics",
                            "required": "1",
                            "type": "edit",
                        },
                        {
                            "caption": "Metric Path",
                            "default": "/metrics",
                            "help": "Enter the path to the metrics, e.g. /metrics",
                            "name": "metric_path",
                            "required": "1",
                            "type": "edit",
                        },
                    ],
                    "name": "metric_info",
                },
            ],
            "help": "Monitors a Prometheus exporter and pulls select metrics.",
            "kind": "mpprometheusexporter",
            "name": "Prometheus Exporter (experimental)",
            "tag": "mpprometheusexporter",
        }


@pytest.mark.asyncio
class TestPrometheusExporterSensorWork:
    async def test_sensor_prometheus_exporter_work(self, prometheus_exporter_sensor, monkeypatch):
        """This test can only be run as part of the whole test suite not as a single test.
        If you want to run only this test, adjust the metrics function.
        """
        monkeypatch.setattr(Aclient, "send", metrics)
        prometheus_exporter_queue = asyncio.Queue()
        await prometheus_exporter_sensor.work(task_data=task_data_prometheus_exporter(), q=prometheus_exporter_queue)
        prometheus_exporter_out = await prometheus_exporter_queue.get()
        assert prometheus_exporter_out == {
            "sensorid": "1234",
            "message": "OK",
            "channel": [
                {
                    "name": "metric_without_timestamp_and_labels - {}",
                    "mode": "float",
                    "value": 12.47,
                    "kind": "Custom",
                    "customunit": "",
                }
            ],
        }

    async def test_sensor_prometheus_exporter_httpx_error(
        self, prometheus_exporter_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Aclient, "send", raise_httpx_httperror)
        prometheus_exporter_queue = asyncio.Queue()
        await prometheus_exporter_sensor.work(task_data=task_data_prometheus_exporter(), q=prometheus_exporter_queue)
        prometheus_exporter_out = await prometheus_exporter_queue.get()
        sensor_exception_message["message"] = "HTTP request failed. See log for details."
        assert prometheus_exporter_out == sensor_exception_message
        logging_mock.exception.assert_called_with("There was an error in httpx.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_prometheus_exporter_key_error(
        self, prometheus_exporter_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Aclient, "send", raise_key_error)
        prometheus_exporter_queue = asyncio.Queue()
        await prometheus_exporter_sensor.work(task_data=task_data_prometheus_exporter(), q=prometheus_exporter_queue)
        prometheus_exporter_out = await prometheus_exporter_queue.get()
        sensor_exception_message["message"] = "Something went wrong. See logs for details."
        assert prometheus_exporter_out == sensor_exception_message
        logging_mock.exception.assert_called_with("Something went wrong.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_prometheus_exporter_value_error(
        self, prometheus_exporter_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Aclient, "send", raise_value_error)
        prometheus_exporter_queue = asyncio.Queue()
        await prometheus_exporter_sensor.work(task_data=task_data_prometheus_exporter(), q=prometheus_exporter_queue)
        prometheus_exporter_out = await prometheus_exporter_queue.get()
        sensor_exception_message["message"] = "Something went wrong. See logs for details."
        assert prometheus_exporter_out == sensor_exception_message
        logging_mock.exception.assert_called_with("Something went wrong.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_prometheus_exporter_casting_error(
        self, prometheus_exporter_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(Aclient, "send", raise_casting_exception)
        prometheus_exporter_queue = asyncio.Queue()
        await prometheus_exporter_sensor.work(task_data=task_data_prometheus_exporter(), q=prometheus_exporter_queue)
        prometheus_exporter_out = await prometheus_exporter_queue.get()
        sensor_exception_message["message"] = "Could not cast the value returned to the correct format."
        assert prometheus_exporter_out == sensor_exception_message
        logging_mock.exception.assert_called_with("Casting error.")
        logging_mock.exception.assert_called_once()


class TestPrometheusExporterSensorMisc:
    def test_cast_method_success(self):
        assert Sensor.cast_to_float(1, "test") == 1.0

    def test_cast_method_error(self):
        with pytest.raises(CastException) as e:
            Sensor.cast_to_float("test", "test")
        assert e.value.args[0] == "Could not convert test for sample test"
