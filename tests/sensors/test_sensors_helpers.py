import pytest

from prtg_pyprobe.utils.validators import ValidationFailedError


class TestSensorData:
    def test_sensor_data_attrs(self, sensor_data):
        sensor_data.message = "test"
        sensor_data.error = "Exception"
        sensor_data.error_code = 1
        assert hasattr(sensor_data, "message")
        assert hasattr(sensor_data, "error")
        assert hasattr(sensor_data, "error_code")
        assert hasattr(sensor_data, "data")

    def test_sensor_data_validation_failed(self, sensor_data):
        sensor_data.message = "test"
        sensor_data.error = "Exception"
        sensor_data.error_code = "1"
        with pytest.raises(ValidationFailedError):
            hasattr(sensor_data, "data")

    def test_sensor_data_channel_attrs(self, sensor_data):
        channel = sensor_data.SensorDataChannel(
            name="test",
            mode="float",
            value=0.1,
            kind="TimeResponse",
            unit="TimeResponse",
            customunit="help",
        )
        assert hasattr(channel, "data")

    def test_sensor_data_add_channel(self, sensor_data):
        sensor_data.message = "test"
        sensor_data.add_channel(
            name="test",
            mode="float",
            value=0.1,
            kind="TimeResponse",
            unit="TimeResponse",
            customunit="help",
        )
        assert sensor_data.data == {
            "sensorid": "1234",
            "message": "test",
            "channel": [
                {
                    "name": "test",
                    "mode": "float",
                    "value": 0.1,
                    "kind": "TimeResponse",
                    "unit": "TimeResponse",
                    "customunit": "help",
                }
            ],
        }


class TestSensorDefinition:
    def test_sensor_definition_add_group_fail(self, sensor_definition):
        try:
            # noinspection PyTypeChecker
            sensor_definition.add_group("bla")
        except sensor_definition.NotASensorDefinitionGroupError as e:
            assert str(e) == "Group has to be of type SensorDefinitionGroup."

    def test_sensor_definition_add_group_field_timeout_success(self, sensor_definition, sensor_definition_group):
        sensor_definition_group.add_field_timeout(1, 2, 3)
        sensor_definition.add_group(sensor_definition_group)
        assert sensor_definition.data == {
            "name": "Testsensor",
            "kind": "testkind",
            "description": "This is a Test",
            "help": "Test Help",
            "tag": "testsensor",
            "groups": [
                {
                    "name": "testgroup",
                    "caption": "TestCaption",
                    "fields": [
                        {
                            "type": "integer",
                            "name": "timeout",
                            "caption": "Timeout (in s)",
                            "required": "1",
                            "default": 1,
                            "minimum": 2,
                            "maximum": 3,
                            "help": "If the reply takes longer than this value the request is aborted and an error "
                            "message"
                            " is triggered. Maximum value is 3 sec. (= 0.05 minutes)",
                        }
                    ],
                }
            ],
            "default": "1",
        }

    def test_sensor_definition_add_custom_group(self, sensor_definition, sensor_definition_group):
        sensor_definition_group.add_field(
            field_type="integer",
            name="testfield",
            caption="Testfield",
            required="1",
            default=1,
            minimum=2,
            maximum=3,
            help="Testhelp",
        )
        sensor_definition.add_group(sensor_definition_group)
        assert sensor_definition.data == {
            "default": "1",
            "description": "This is a Test",
            "groups": [
                {
                    "caption": "TestCaption",
                    "fields": [
                        {
                            "caption": "Testfield",
                            "default": 1,
                            "help": "Testhelp",
                            "maximum": 3,
                            "minimum": 2,
                            "name": "testfield",
                            "required": "1",
                            "type": "integer",
                        }
                    ],
                    "name": "testgroup",
                }
            ],
            "help": "Test Help",
            "kind": "testkind",
            "name": "Testsensor",
            "tag": "testsensor",
        }
