import asyncio
import os
import socket

import aioping
import pytest

from prtg_pyprobe.sensors import sensor_ping as module
from prtg_pyprobe.sensors.sensor import SensorBase
from tests.conftest import asyncio_open_connection


async def ping_icmp_success(*args, **kwargs):
    return 1234


async def ping_connection_error(*args, **kwargs):
    raise ConnectionError


async def ping_timeout_error(*args, **kwargs):
    raise TimeoutError


async def ping_async_timeout_error(*args, **kwargs):
    raise asyncio.TimeoutError


async def ping_socket_error(*args, **kwargs):
    raise socket.gaierror


async def ping_os_error(*args, **kwargs):
    raise OSError


def os_getuid():
    return 0


def is_admin():
    return True


def is_not_admin():
    return False


def task_data_ping(ptype="tcp"):
    return {
        "sensorid": "1234",
        "timeout": "5",
        "host": "127.0.0.1",
        "ping_count": 3,
        "target_port": "443",
        "ping_type": ptype,
    }


class TestPingSensorProperties:
    def test_sensor_ping_base_class(self, ping_sensor):
        assert type(ping_sensor).__bases__[0] is SensorBase

    def test_sensor_ping_name(self, ping_sensor):
        assert ping_sensor.name == "Ping (TCP/ICMP)"

    def test_sensor_ping_kind(self, ping_sensor):
        assert ping_sensor.kind == "mpping"

    def test_sensor_ping_definition(self, ping_sensor):
        assert ping_sensor.definition == {
            "description": "Monitors the availability of a target using TCP or ICMP",
            "groups": [
                {
                    "caption": "Ping Settings",
                    "fields": [
                        {
                            "caption": "Timeout (in s)",
                            "default": 5,
                            "help": "If the reply takes longer than this value "
                            "the request is aborted and an error message "
                            "is triggered. Maximum value is 300 sec. (= "
                            "5.0 minutes)",
                            "maximum": 300,
                            "minimum": 1,
                            "name": "timeout",
                            "required": "1",
                            "type": "integer",
                        },
                        {
                            "caption": "Ping Count",
                            "default": 4,
                            "help": "Enter the count of Ping requests PRTG will "
                            "send to the device during an interval",
                            "maximum": 20,
                            "minimum": 1,
                            "name": "ping_count",
                            "required": "1",
                            "type": "integer",
                        },
                        {
                            "caption": "Port (TCP mode only)",
                            "default": 443,
                            "help": "This is the port that you want to monitor if "
                            "it's open or closed. Only used for TCP Ping",
                            "maximum": 65534,
                            "minimum": 1,
                            "name": "target_port",
                            "required": "1",
                            "type": "integer",
                        },
                        {
                            "caption": "Ping Type",
                            "default": "tcp",
                            "help": "Choose your Ping type. Note that for ICMP " "the pyprobe must run as root!",
                            "name": "ping_type",
                            "options": {"icmp": "ICMP", "tcp": "TCP"},
                            "required": "1",
                            "type": "radio",
                        },
                    ],
                    "name": "pingsettings",
                }
            ],
            "help": "Monitors the availability of a target using TCP or ICMP",
            "kind": "mpping",
            "name": "Ping (TCP/ICMP)",
            "tag": "mppingsensor",
        }


@pytest.mark.asyncio
class TestPingSensorWork:
    async def test_sensor_ping_work_tcp(self, ping_sensor, monkeypatch):
        monkeypatch.setattr(asyncio, "wait_for", asyncio_open_connection)
        ping_queue = asyncio.Queue()
        await ping_sensor.work(task_data=task_data_ping(ptype="tcp"), q=ping_queue)
        out = await ping_queue.get()
        assert out["message"] == "OK. Ping Mode tcp"
        assert out["sensorid"] == "1234"
        assert len(out["channel"]) == 4

    async def test_sensor_ping_work_icmp(self, ping_sensor, monkeypatch):
        monkeypatch.setattr(ping_sensor, "_is_admin", is_admin)
        monkeypatch.setattr(aioping, "ping", ping_icmp_success)
        ping_queue = asyncio.Queue()
        await ping_sensor.work(task_data=task_data_ping(ptype="icmp"), q=ping_queue)
        out = await ping_queue.get()
        assert out["message"] == "OK. Ping Mode icmp"
        assert out["sensorid"] == "1234"
        assert len(out["channel"]) == 4

    async def test_sensor_ping_work_icmp_permission_error(
        self, ping_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(ping_sensor, "_is_admin", is_not_admin)
        monkeypatch.setattr(aioping, "ping", ping_icmp_success)
        ping_queue = asyncio.Queue()
        await ping_sensor.work(task_data=task_data_ping(ptype="icmp"), q=ping_queue)
        out = await ping_queue.get()
        sensor_exception_message["message"] = "The pyprobe must run as root/administrator to use ICMP Ping"
        assert out == sensor_exception_message
        logging_mock.exception.assert_called_with("ICMP Ping uses RAW sockets. You must be root to use them.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_ping_work_icmp_connection_error(
        self, ping_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(ping_sensor, "_is_admin", is_admin)
        monkeypatch.setattr(aioping, "ping", ping_connection_error)
        ping_queue = asyncio.Queue()
        await ping_sensor.work(task_data=task_data_ping(ptype="icmp"), q=ping_queue)
        out = await ping_queue.get()
        sensor_exception_message["message"] = "Ping check failed. See log for details"
        assert out == sensor_exception_message
        logging_mock.exception.assert_called_with("There has been a connection error.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_ping_work_icmp_timeout_error(
        self, ping_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(ping_sensor, "_is_admin", is_admin)
        monkeypatch.setattr(aioping, "ping", ping_timeout_error)
        ping_queue = asyncio.Queue()
        await ping_sensor.work(task_data=task_data_ping(ptype="icmp"), q=ping_queue)
        out = await ping_queue.get()
        sensor_exception_message["message"] = "Ping Timeout. See log for details"
        assert out == sensor_exception_message
        logging_mock.exception.assert_called_with("There has been a timeout.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_ping_work_icmp_socket_error(self, ping_sensor, monkeypatch, mocker, sensor_exception_message):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(ping_sensor, "_is_admin", is_admin)
        monkeypatch.setattr(aioping, "ping", ping_socket_error)
        ping_queue = asyncio.Queue()
        await ping_sensor.work(task_data=task_data_ping(ptype="icmp"), q=ping_queue)
        out = await ping_queue.get()
        sensor_exception_message["message"] = "Ping check failed. See log for details"
        assert out == sensor_exception_message
        logging_mock.exception.assert_called_with("There has been a connection error.")
        logging_mock.exception.assert_called_once()

    async def test_sensor_ping_work_os_error(self, ping_sensor, monkeypatch, mocker, sensor_exception_message):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(ping_sensor, "_is_admin", is_admin)
        monkeypatch.setattr(aioping, "ping", ping_os_error)
        ping_queue = asyncio.Queue()
        await ping_sensor.work(task_data=task_data_ping(ptype="icmp"), q=ping_queue)
        out = await ping_queue.get()
        sensor_exception_message["message"] = "Target device/port probably not available."
        assert out == sensor_exception_message
        logging_mock.exception.assert_called_with(
            "Something on the network level went wrong. " "Target host not reachable on selected port."
        )
        logging_mock.exception.assert_called_once()

    async def test_sensor_ping_work_async_timeout_error(
        self, ping_sensor, monkeypatch, mocker, sensor_exception_message
    ):
        logging_mock = mocker.patch.object(module, "logging")
        monkeypatch.setattr(ping_sensor, "_is_admin", is_admin)
        monkeypatch.setattr(aioping, "ping", ping_async_timeout_error)
        ping_queue = asyncio.Queue()
        await ping_sensor.work(task_data=task_data_ping(ptype="icmp"), q=ping_queue)
        out = await ping_queue.get()
        sensor_exception_message["message"] = "Ping Timeout. See log for details"
        assert out == sensor_exception_message
        logging_mock.exception.assert_called_with("There has been a timeout.")
        logging_mock.exception.assert_called_once()

    def test__is_admin_linux(self, monkeypatch):
        monkeypatch.setattr(os, "getuid", os_getuid)
        assert module.Sensor._is_admin()
